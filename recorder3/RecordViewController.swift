//
//  RecordViewController.swift
//  recorder3
//
//  Created by Johan Graucob on 2015-08-06.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

import UIKit
import AVFoundation

class RecordViewController: UIViewController, AVAudioRecorderDelegate {

    @IBOutlet weak var buttonRecord: UIButton!
    @IBOutlet weak var buttonStop: UIButton!
    @IBOutlet weak var labelRecording: UILabel!
    
    var audioRecorder:AVAudioRecorder!
    var recordedAudio:RecordedAudio!

    override func viewDidLoad() {
 
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        buttonStop.hidden = true
        labelRecording.hidden = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func startRecording(sender: UIButton) {
        println("inside startRecording")
        buttonStop.hidden = false
        buttonRecord.enabled = false
        labelRecording.hidden = false
        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        let recordingName = "rec.wav"
        let pathArray = [dirPath, recordingName]
        let filePath = NSURL.fileURLWithPathComponents(pathArray)
        println(filePath)
        
        var session = AVAudioSession.sharedInstance()
        session.setCategory(AVAudioSessionCategoryPlayAndRecord, error: nil)
        audioRecorder = AVAudioRecorder(URL: filePath, settings: nil, error: nil)
        audioRecorder.delegate = self
        audioRecorder.meteringEnabled = true
        audioRecorder.record()
        
    }
    
    @IBAction func stopRecording(sender: UIButton) {
        println("inside stopRecording")
        buttonRecord.enabled = true
        labelRecording.hidden = true
        audioRecorder.stop()
        var audioSession = AVAudioSession.sharedInstance();
        audioSession.setActive(false, error: nil)
    }
    
    
    /*
        Called before the actual segue takes place from audioRecorderdidFinishRecording
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "stopRecording"){
            let playSoundsVC: PlaySoundsViewController = segue.destinationViewController as PlaySoundsViewController
            let data = sender as RecordedAudio
            playSoundsVC.receivedAudio = data
        }
    }
    
    
    /*
        Called upon when the AVAudioRecorder finishes its recording.
    */
     func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        println("inside audioRecorderDiDFinishRecording")
        
        // this if statements checks for the flag boolean sent as parameter to the function.
        if(flag){
            
            recordedAudio = RecordedAudio()
            recordedAudio.filePathUrl = recorder.url
            recordedAudio.title = recorder.url.lastPathComponent
            
            //recordedAudio is the object that actuall initiates the segue
            self.performSegueWithIdentifier("stopRecording", sender: recordedAudio)
        
        }else{
            println("Recording was not successfull")
            buttonRecord.enabled = true
            buttonStop.hidden = true
        }
        
    }

}

