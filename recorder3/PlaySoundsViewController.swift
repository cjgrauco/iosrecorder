//
//  PlaySoundsViewController.swift
//  recorder3
//
//  Created by Johan Graucob on 2015-08-07.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

import UIKit
import AVFoundation

class PlaySoundsViewController: UIViewController {
    @IBOutlet weak var buttonSlow: UIButton!
    @IBOutlet weak var buttonStop: UIButton!
 
    var audioPlayer: AVAudioPlayer!
    var receivedAudio:RecordedAudio!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        buttonStop.hidden = true
        
//        if var filePath = NSBundle.mainBundle().pathForResource("movie_quote", ofType: "mp3"){
//            var filePathUrl = NSURL.fileURLWithPath(filePath)
//           
//        }
//        
        audioPlayer = AVAudioPlayer(contentsOfURL: receivedAudio.filePathUrl, error: nil)
        audioPlayer.enableRate = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func slowRecording(sender: UIButton) {
        println("inside slowRecording")
        buttonStop.hidden = false
        audioPlayer.rate = 0.5
        audioPlayer.currentTime = 0.0
        audioPlayer.play()
    }
    
    @IBAction func stopPlayback(sender: UIButton) {
        audioPlayer.stop()
        buttonStop.hidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
