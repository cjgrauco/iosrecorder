//
//  RecordedAudio.swift
//  recorder3
//
//  Created by Johan Graucob on 2015-08-07.
//  Copyright (c) 2015 Johan Graucob. All rights reserved.
//

import Foundation


class RecordedAudio: NSObject {
    var filePathUrl: NSURL!
    var title: String!
    
}